import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/map',
    name: 'Map',
    component: () => import('../views/Map.vue')
  },
  {
    path: '/dataMap',
    name: 'DataMap',
    component: () => import('../views/DataMap.vue')
  },
  {
    path: '/foreignData',
    name: 'ForeignData',
    component: () => import('../views/ForeignData.vue')
  },
  {
    path: '/current',
    name: 'Current',
    component: () => import('../views/Current.vue')
  },
  {
    path: '/hosp',
    name: 'Hosp',
    component: () => import('../views/Hosp.vue')
  },
  {
    path: '/hospMap',
    name: 'HospMap',
    component: () => import('../views/HospMap.vue')
  },
  {
    path: '/userManager',
    name: 'UserManager',
    component: () => import('../views/UserManager.vue')
  },
  {
    path: '/infoList',
    name: 'InfoList',
    component: () => import('../views/InfoList.vue')
  },
  {
    path: '/article',
    name: 'Article',
    component: () => import('../views/Article.vue')
  },
  {
    path: '/diffuse',
    name: 'Diffuse',
    component: () => import('../views/Diffuse.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/hospital',
    name: 'Hospital',
    component: () => import('../views/Hospital.vue')
  },
  {
    path: '/list',
    name: 'List',
    component: () => import('../views/EpidemicData/List.vue')
  },
  {
    path: '/up',
    name: 'Up',
    component: () => import('../views/EpidemicData/Up.vue')
  }
]

const router = new VueRouter({
  // mode: 'history',
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
