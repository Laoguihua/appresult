/*
 * @Author: gui
 * @Date: 2020-12-29 15:30:25
 * @Last Modified by: gui
 * @Last Modified time: 2021-01-06 16:01:55
 * 配置axios
 */

import axios from 'axios';
import qs from 'qs';

axios.defaults.baseURL = 'http://159.75.29.60:8001'

axios.interceptors.request.use(config => {
    config.headers = {
        ...config.headers,
        Authorization: sessionStorage.getItem("token")
    }
    if (config.method == 'post' && config.url != "/user/login") {
        config.data = qs.stringify(config.data);
    }


    return config;
}, error => {
    return Promise.reject(error);
});

axios.interceptors.response.use(response => {
    let res = {
        ...response,
        data: response.data.data,
        status: response.data.status,
        statusText: response.data.message,
        timestamp: response.data.timestamp
    }

    return res;
}, error => {
    return Promise.reject(error);
});


export default axios;





