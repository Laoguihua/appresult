# Appresult

#### 介绍
全国疫情可视化平台

#### 软件架构
全国疫情防控监控平台开发，包括疫情动态，疫情地图，实时疫情图表，疫情数据统计可视化界面，定点医院的追踪，定点医院信息管理，新闻资讯的展示，分类管理，疫情数据的上报和展示，用户登陆登出，信息及权限的管理，实时天气预报等功能。前端项目环境部署，在团队开发过程中中使用远程仓库gitee进行项目的管理和开发。
项目采用Vue + Vue Router + Vuex + ElementUI + Axios + Node.js等框架开发，采用ECharts/HighCharts可视化插件和高德地图API。该项目方便在已有数据的基础上进行可视化显示，方便用户掌握疫情变化趋势，从而进行对应的防控措施。


#### 安装教程

1.  Linux安装Java
2.  安装apache 2.4
3.  安装mysql
4.  远程连接mysql，初始化数据仓库，为项目提供数据源
5.  部署web项目

#### 使用说明

1.  用户登陆登出及权限控制，页面顶部功能扩展
2.  疫情动态，疫情地图
3.  疫情数据
4.  疫情管理
5.  实时疫情
6.  用户基础配置，抗议祝福
7.  数据统计
8.  定点医院

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
